LPC1347 Core Board

A simple core board for the LPC1347 in LQFP48 package to use it in through-hole projects or on breadboards.
All pins (but Xtal) are on the connector pins. The Xtal is supposed to be placed on board. Additional SWD debug connector.
Schematics and Layout created with DipTrace. Gerber files are included.
There's also a test software project that toggles all the GPIO pins where the pin number is the pulse length in milliseconds.

---------------------------------------------------------------
Copyright 2015 Volker Oth - VolkerOth(at)gmx.de
Licensed under the Creative Commons Attribution 4.0 license
http://creativecommons.org/licenses/by/4.0/

Everything in this repository is distributed on an "AS IS" BASIS, WITHOUT
WARRANTIES OF ANY KIND, either express or implied.
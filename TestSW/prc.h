/** Processor library
	Provide abstraction on CPU level
	LPX13xx ARM Cortex M3
	----------------------------------------------------------
	Copyright 2012 Volker Oth
	Licensed under the Creative Commons Attribution 4.0 license
	http://creativecommons.org/licenses/by/4.0/

	Software is distributed on an "AS IS" BASIS, WITHOUT
	WARRANTIES OF ANY KIND, either express or implied.
*/

#ifndef PRC_H
#define PRC_H

#include "global.h"

/** Get most significant set bit.
   1000_1111_1111_1111_1111_1111_1111_1111 -> 32 -> highest bit is set
   0100_1111_1111_1111_1111_1111_1111_1111 -> 31 -> 2nd highest bit is set
   0000_0000_0000_0000_0000_0000_0000_0001 ->  1 -> least significant bit is set
   0000_0000_0000_0000_0000_0000_0000_0000 ->  0 -> no bit is set
   @param val 32bit value to check
   @return position of most significant bit set (1..31) or 0 if no bit set
*/
static __INLINE u32 PRC_GetMSB(u32 val) {
	__ASM volatile("clz %0, %1" : "=r" (val) : "r" (val));
	return 32-val;
}

/** Reverse bit order of 32bit number.
	1011_0100_1110_0001_0000_1100_0010_0011 ->  1100_0100_0011_0000_1000_0111_0010_1101
	@param val 32bit value to reserve.
	@return value in reversed bit order
*/
static __INLINE u32 PRC_ReverseBitorder(u32 val) {
	__ASM volatile("rbit %0, %1" : "=r" (val) : "r" (val));
	return val;
}

/** Reverse byte order of 32bit number.
	0x12345678 -> 0x78563412
	@param val 32bit value to reserve.
	@return value in reversed byte order
*/
static __INLINE u32 PRC_ReverseByteOrder(u32 val) {
	__ASM volatile("rev %0, %1" : "=r" (val) : "r" (val));
	return val;
}

/** Reverse byte order of 32bit number per 16bit.
	0x12345678 -> 0x34127856
	@param val 32bit value to reserve.
	@return value in reversed byte order
*/
static __INLINE u32 PRC_ReverseByteOrderW(u32 val) {
	__ASM volatile("rev16 %0, %1" : "=r" (val) : "r" (val));
	return val;
}

/** 32bit unsigned division.
	@param val value to divide
	@param div value to divide with
	@return val divided by div (32bit)
*/
static __INLINE u32 PRC_Div_u32_u32(u32 val, u32 div) {
	__ASM volatile ("udiv %0, %1" : "+&r" (val) : "r" (val), "r" (div) : "cc");
	return val;
}

/** 32bit x 32bit unsigned multiplication with 64bit result.
	@param val1 value 1 to multiply
	@param val1 value 1 to multiply
	@return val1 multiplied by val2 (64bit)
*/
static __INLINE u64 PRC_Mul_u32_u32(u32 val1, u32 val2) {
	u64 res;
	__ASM volatile ("umull %Q0, %R0, %1, %2" : "=&r" (res) : "r" (val1), "r" (val2) : "cc");
	return res;
}

/** 32bit x 32bit unsigned multiply and add (MAC) with 64bit result.
	@param val1 value 1 to multiply (32bit)
	@param val1 value 1 to multiply (32bit)
	@param add 64bit value to add to result of multiplication
	@return val1 multiplied by val2 (64bit) plus add (64bit)
*/
static __INLINE u64 PRC_Mac_u32_u32(u32 val1, u32 val2, u64 add) {
	__ASM volatile ("umlal %Q0, %R0, %1, %2" : "+&r" (add) : "r" (val1), "r" (val2) : "cc");
	return add;
}

/** 32bit x 32bit signed multiplication with 64bit result.
	@param val1 value 1 to multiply
	@param val1 value 1 to multiply
	@return val1 multiplied by val2 (64bit)
*/
static __INLINE s64 PRC_Mul_s32_s32(s32 val1, s32 val2) {
	s64 res;
	__ASM volatile ("smull %Q0, %R0, %1, %2" : "=&r" (res) : "r" (val1), "r" (val2) : "cc");
	return res;
}

/** 32bit x 32bit signed multiply and add (MAC) with 64bit result.
	@param val1 value 1 to multiply (32bit)
	@param val1 value 1 to multiply (32bit)
	@param add 64bit value to add to result of multiplication
	@return val1 multiplied by val2 (64bit) plus add (64bit)
*/
static __INLINE s64 PRC_Mac_s32_s32(s32 val1, s32 val2, s64 add) {
	__ASM volatile ("smlal %Q0, %R0, %1, %2" : "+&r" (add) : "r" (val1), "r" (val2) : "cc");
	return add;
}

extern volatile int prc_irq_nesting_lvl;

/** Nested interrupt enable */
#define PRC_Int_Enable()  { if (--prc_irq_nesting_lvl == 0) __ASM volatile ("cpsie i"); }
/** Nested interrupt disable */
#define PRC_Int_Disable() { if (prc_irq_nesting_lvl++ == 0) __ASM volatile ("cpsid i"); }

/** Calculate bit band base address from address */
#define BITBAND_BASE(a) (((u32)(a)&0x60000000)+0x2000000)
/** Calculate bit band offset from address */
#define BITBAND_OFS(a) ((u32)(a)-((u32)(a)&0x60000000))

/** Return bit band address of address a and bit number b [0..31] */
#define BITBAND_ADR(a,b) (BITBAND_BASE(a) + 32*BITBAND_OFS((u32)a)+4*(b))

/** Set bit via bit-banding.
	NOTE: bit-banding works only for 0x20000000-0x200FFFFF and 0x40000000-0x400FFFFF
	This excludes the 1st SRAM block (32k: 0x10000000-0x10007FFF), also the DMA Controller,
	    Ethernet interface and USB interface.
	This includes the 2nd SRAM block (32k: 0x2007C000-0x20083FFF), GPIO (0x2009C000 - 0x2009FFFF),
	    most peripherals (APB) (0x40000000-0x400FFFFF)
	@param a address [0x20000000..0x200FFFFF] or [0x40000000..0x400FFFFF]
	@param b bit number [0..31]
	@param v value [0..1]
*/
#define BITBAND_SET_BIT(a,b,v) {(*(u32*)BITBAND_ADR(a,b)) = (u32)(v);}

/** Read bit via bit-banding.
	NOTE: bit-banding works only for 0x20000000-0x200FFFFF and 0x40000000-0x400FFFFF
	This excludes the 1st SRAM block (32k: 0x10000000-0x10007FFF), also the DMA Controller,
	    Ethernet interface and USB interface.
	This includes the 2nd SRAM block (32k: 0x2007C000-0x20083FFF), GPIO (0x2009C000 - 0x2009FFFF),
	    most peripherals (APB) (0x40000000-0x400FFFFF)
	@param a address [0x20000000..0x200FFFFF] or [0x40000000..0x400FFFFF]
	@param b bit number [0..31]
	@return bit value [0..1]
*/
#define BITBAND_GET_BIT(a,b,v) (*(u32*)BITBAND_ADR(a,b))

/** Divide of unsigned 40bit integer by unsigned 32bit integer.
	@param val 64bit number that contains a 40bit number [0..0xffffffffff]
	@param div 32bit divider
	@result val divided by div (32bit - saturated to 32bit if > 0xffffffff)
 */
static __INLINE u32 div_u40_u24(u64 val, u32 div) {
	u32 hi, hi_rem;
	u64 hi_out;
	hi = (u32)(val >> 8);
	hi_out = (u64)(hi / div);
	hi_rem = (u64)(hi % div); // hi_rem = hi - hi_out*div;
	if (hi_out <= (u64)0xffffff) {
		// only continue if the shifted value fits into 32bit
		hi_out <<= 8;      // 64bit shift
		// the remainder must fit into 24bit if the divisor was
		// 24bit. So when shifting up, it must fit into 32bit.
		hi_rem <<= 8;      // 32bit shift
		// when adding the low part (<=255 by definition)
		// there can't be an overflow, as the lower 8bit
		// are empty after shifting up.
		hi_rem += (u8)val; // 32bit addition
		hi_out += (u64)(hi_rem/div); // 32bit division, 64bit addition
		// saturate if result doesn't fit into 32bit
		if (hi_out > (u64)0xffffffff)
			hi_out = (u64)0xffffffff;
	} else
		// saturate if result doesn't fit into 32bit
		hi_out = (u64)0xffffffff;
	return (u32)hi_out;
}

/** Calculate percentage of a 32bit value v32 where percentage pct15 is given in 100% == 0x8000
	Note: (0xffffffff>>15)*0x8000 just fits into 32bit -> no overflow as long as pct15 is <= 0x8000
	@param v32  unsigned 32bit value
	@param pct15 percentage in PCT15 resolution [0..0x8000], where 100% == 0x8000
	@result percentage of value (v32*pct15/0x8000)
*/
#define u32_pct15(v32, pct15) ( (((u32)(v32)>>15)*(u32)(pct15)) + ((((u32)(v32)&0x7fff)*(u32)(pct15))/0x8000) )

/* Mutex */

/** Acquire mutual exclude.
	@param mutex Mutex handle
 */
__INLINE static void MUTEX_Lock(mutex_t *mutex) {
   __ASM volatile ("         LDR r1, =1\n\t"
		           "START%=: LDREX  r2, [%0]\n\t"
		           "         CMP r2, #1\n\t"            // test if mutex is locked
		           "         ITT NE\n\t"                // if/then needed on thumb
		           "         STREXNE r2, r1, [%0]\n\t"  // try to lock it only if not locked
		           "         CMPNE r2, #1\n\t"          // check if lock failed
		           "         BEQ START%=\n\t\n\t"       // retry if failed
		           "         DMB" : "=r" (mutex) : "0" (mutex) : "cc", "r1", "r2");
}

/** Release mutual exclude.
	@param mutex Mutex handle
 */
__INLINE static void MUTEX_Unlock(mutex_t *mutex) {
   __ASM volatile ("LDR r1, =0\n\t"
		           "DMB\n\t"
		           "STR r1, [%0]" : "=r" (mutex) : "0" (mutex) : "r1");
}

#endif


/** Priority Wrapper
	Provide means to wrap down an interrupt with higher priority to a base priority
	LPX13xx ARM Cortex M3
	----------------------------------------------------------
	Copyright 2012 Volker Oth
	Licensed under the Creative Commons Attribution 4.0 license
	http://creativecommons.org/licenses/by/4.0/

	Software is distributed on an "AS IS" BASIS, WITHOUT
	WARRANTIES OF ANY KIND, either express or implied.
*/

#include "global.h"
#include "prc.h"
#include "priowrap.h"

/** Double buffered priority wrapper table */
Prio_Wrap_Table_t prio_wrap_table[2];
/** Index of active table */
volatile u8       prio_wrap_table_active;


/** Initialize priority wrapper */
void PrioWrap_Init(void) {
	NVIC_SetPriority(PrioWrap_IRQn, PRIOWRAP_IQR_PRIO);     /* set to low prio */
}

/** Wrap function other (usually lower) priority.
	@param func function pointer of type Prio_Wrap_Func_t (void return, u32 param)
	@param param unsigned 32bit parameter
	@return 1 if ok, 0 if failed
	*/
int PrioWrap_Function(Prio_Wrap_Func_t func, u32 param) {
	int retval=0;
	int idx;
	Prio_Wrap_Table_t *table = &prio_wrap_table[prio_wrap_table_active];

	// Protect against interruption from handler (if wrapped up)
	// or from another call to this very functions from a higher level
	PRC_Int_Disable();
	idx = table->idx;
	if (idx < PRIO_WRAP_MAX_ENTRIES) {
		table->idx = idx+1;
		table->entries[idx].func = func;
		table->entries[idx].param = param;
		Prio_Wrap_Create_IRQ();
		retval = 1;
	}
	PRC_Int_Enable();

	return retval;
}

/** IRQ Handler for Priority Wrapper - assumed to run on low priority */
void PrioWrap_Handler(void) {
	int idx;

	Prio_Wrap_Table_t *table = &prio_wrap_table[prio_wrap_table_active];
	// swap table  - no lock needed
	prio_wrap_table_active = (!prio_wrap_table_active) & 1;
	// now new wrapper requests will be stored in the other table
	// an we can execute all requests in the old table
	int max_idx = table->idx;
	table->idx = 0; // reset idx
	// note: the oldest entry is at position 0 and is executed first
	for (idx=0; idx<max_idx; idx++) {
		// call function from the list
		(*(table->entries[idx].func))((table->entries[idx]).param);
	}
}

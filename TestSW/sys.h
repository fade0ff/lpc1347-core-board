/** System configuration
	Configuration of system resources (CPU clock, prescalers etc.)
	LPX13xx ARM Cortex M3
	----------------------------------------------------------
	Copyright 2012 Volker Oth
	Licensed under the Creative Commons Attribution 4.0 license
	http://creativecommons.org/licenses/by/4.0/

	Software is distributed on an "AS IS" BASIS, WITHOUT
	WARRANTIES OF ANY KIND, either express or implied.
*/

#ifndef SYS_H
#define SYS_H

#include "sys_cnf.h"

#define SYS_CLK_SYS        (1<<0)
#define SYS_CLK_ROM        (1<<1)
#define SYS_CLK_RAM0       (1<<2)
#define SYS_CLK_FLASHREG   (1<<3)
#define SYS_CLK_FLASHARRAY (1<<4)
#define SYS_CLK_I2C        (1<<5)
#define SYS_CLK_GPIO       (1<<6)
#define SYS_CLK_CT16B0     (1<<7)
#define SYS_CLK_CT16B1     (1<<8)
#define SYS_CLK_CT32B0     (1<<9)
#define SYS_CLK_CT32B1     (1<<10)
#define SYS_CLK_SSP0       (1<<11)
#define SYS_CLK_USART      (1<<12)
#define SYS_CLK_ADC        (1<<13)
#define SYS_CLK_USB        (1<<14)
#define SYS_CLK_WWDT       (1<<15)
#define SYS_CLK_IOCON      (1<<16)
#define SYS_CLK_SSP1       (1<<18)
#define SYS_CLK_PINT       (1<<19)
#define SYS_CLK_GROUP0INT  (1<<23)
#define SYS_CLK_GROUP1INT  (1<<24)
#define SYS_CLK_RAM1       (1<<26)
#define SYS_CLK_USBSRAM    (1<<27)


#define SYS_CLK_SYS_CFG(x)         (((x)&1)<<0)
#define SYS_CLK_ROM_CFG(x)         (((x)&1)<<1)
#define SYS_CLK_RAM0_CFG(x)        (((x)&1)<<2)
#define SYS_CLK_FLASHREG_CFG(x)    (((x)&1)<<3)
#define SYS_CLK_FLASHARRAY_CFG(x)  (((x)&1)<<4)
#define SYS_CLK_I2C_CFG(x)         (((x)&1)<<5)
#define SYS_CLK_GPIO_CFG(x)        (((x)&1)<<6)
#define SYS_CLK_CT16B0_CFG(x)      (((x)&1)<<7)
#define SYS_CLK_CT16B1_CFG(x)      (((x)&1)<<8)
#define SYS_CLK_CT32B0_CFG(x)      (((x)&1)<<9)
#define SYS_CLK_CT32B1_CFG(x)      (((x)&1)<<10)
#define SYS_CLK_SSP0_CFG(x)        (((x)&1)<<11)
#define SYS_CLK_USART_CFG(x)       (((x)&1)<<12)
#define SYS_CLK_ADC_CFG(x)         (((x)&1)<<13)
#define SYS_CLK_USB_CFG(x)         (((x)&1)<<14)
#define SYS_CLK_WWDT_CFG(x)        (((x)&1)<<15)
#define SYS_CLK_IOCON_CFG(x)       (((x)&1)<<16)
#define SYS_CLK_SSP1_CFG(x)        (((x)&1)<<18)
#define SYS_CLK_PINT_CFG(x)        (((x)&1)<<19)
#define SYS_CLK_GROUP0INT_CFG(x)   (((x)&1)<<23)
#define SYS_CLK_GROUP1INT_CFG(x)   (((x)&1)<<24)
#define SYS_CLK_RAM1_CFG(x)        (((x)&1)<<26)
#define SYS_CLK_USBSRAM_CFG(x)     (((x)&1)<<27)


#define SYS_FRQ_PLL (SYS_FRQ_OSC*SYS_MSEL_CFG)

/* Peripheral clocks */
#define SYS_CLK_SSP0_FRQ    (SYS_FRQ_PLL/SYS_CLK_SSP0_DIV)
#define SYS_CLK_SSP1_FRQ    (SYS_FRQ_PLL/SYS_CLK_SSP1_DIV)
#define SYS_CLK_TRACE_FRQ   (SYS_FRQ_PLL/SYS_CLK_TRACE_DIV)
#define SYS_CLK_SYSTICK_FRQ (SYS_FRQ_PLL/SYS_CLK_SYSTICK_DIV)
#define SYS_CLK_UART_FRQ    (SYS_FRQ_PLL/SYS_CLK_UART_DIV)

/* Reset Control */
#define SYS_PRESETCTRL_SSP0_RST_N_CFG(x)  (((x)&1)<<0) /**< 0:SSP0 is reset, 1: SSP0 reset de-asserted. */
#define SYS_PRESETCTRL_I2C_RST_N_CFG(x)   (((x)&1)<<1) /**< 0:I2C is reset, 1: I2C reset de-asserted. */
#define SYS_PRESETCTRL_SSP1_RST_N_CFG(x)  (((x)&2)<<0) /**< 0:SSP0 is reset, 1: SSP0 reset de-asserted. */

/* PLL setup */
#if SYS_MSEL_CFG<1 || SYS_MSEL_CFG>32
#error "MSEL value for PLL setup out of range"
#endif

#if !(SYS_PSEL_CFG==1 || SYS_PSEL_CFG==2 || SYS_PSEL_CFG==4 || SYS_PSEL1_CFG==8)
#error "PSEL value for PLL1 setup out of range"
#endif

#if SYS_PSEL_CFG==8
#define SYS_PSEL_CFG_ 3
#else
#define SYS_PSEL_CFG_ (SYS_PSEL_CFG/2)
#endif

#define PLLCFG_CFG (((SYS_MSEL_CFG - 1)&0x1f) | (((SYS_PSEL_CFG_)&3)<<5))



/* USB PLL setup */
#if SYS_MSEL_USB_CFG<1 || SYS_MSEL_USB_CFG>32
#error "MSEL value for USB PLL setup out of range"
#endif

#if !(SYS_PSEL_USB_CFG==1 || SYS_PSEL_USB_CFG==2 || SYS_PSEL_USB_CFG==4 || SYS_PSEL_USB_CFG==8)
#error "PSEL value for USB PLL setup out of range"
#endif

#if SYS_PSEL_USB_CFG==8
#define SYS_PSEL_USB_CFG_ 3
#else
#define SYS_PSEL_USB_CFG_ (SYS_PSEL_USB_CFG/2)
#endif

#define USBPLLCFG_CFG (((SYS_MSEL_USB_CFG - 1)&0x1f) | (((SYS_PSEL_USB_CFG_)&3)<<5))

#endif

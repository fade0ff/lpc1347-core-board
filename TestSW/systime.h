/** System time library
	Abstraction of global timer/delay functions (actual implementation using RIT peripheral)
	LPX13xx ARM Cortex M3
	----------------------------------------------------------
	Copyright 2012 Volker Oth
	Licensed under the Creative Commons Attribution 4.0 license
	http://creativecommons.org/licenses/by/4.0/

	Software is distributed on an "AS IS" BASIS, WITHOUT
	WARRANTIES OF ANY KIND, either express or implied.
*/

#ifndef SYSTEMTIME_H
#define SYSTEMTIME_H

#include "global.h"
#include "sys.h"
#include "rit.h"
#include "int_cnf.h"

#define SYSTEMTIME_USE_INLINE 0                         /**< Implement system time wait function as inline instead of macros */

#define NS_TO_TICKS(t, f) (((u64)(t)*(f))/1000000000UL) /**< Convert nanoseconds to ticks.  t: time in nanoseconds,  f: timer frequency */
#define US_TO_TICKS(t, f) (((u64)(t)*(f))/1000000UL)    /**< Convert microseconds to ticks. t: time in microseconds, f: timer frequency */
#define MS_TO_TICKS(t, f) (((u64)(t)*(f))/1000UL)       /**< Convert milliseconds to ticks. t: time in milliseconds, f: timer frequency */

#define SYSTIME_FRQ SYS_FRQ_CORE                        /**< Internal timer frequency */

#define SYSTIME_IRQHandler RIT_IRQHandler               /**< Use RIT IRQ handler as SysTime IRQ handler */

/** Clear pending interrupt request for system timer */
#define SYSTIME_ClearRequest() RIT_ClearIntRequest()

/** Initialize the system timer driver. Must be called at system startup before using other functions. */
static __INLINE void SYSTIME_Init(void) {
	RIT_SetControl(RIT_CTRL_HALT_DBG|RIT_CTRL_ENABLE);
}

/** Setup event at a given timer value.
	@param t 32bit timer value in internal tick resolution.
 */
static __INLINE void SYSTIME_SetupEvent(u32 t) {
	RIT_SetCompare(t);
}

/** Setup event in a given time.
	@param t 32bit delta time in internal tick resolution. Use SYSTIME_XsToTick functions to convert.
 */
static __INLINE void SYSTIME_SetupEventDelta(u32 delta) {
	RIT_SetCompare(RIT_GetCounter() + delta);
}

/** Enable the time event.
	@param enable 0: disable, 1: enable
 */
static __INLINE void SYSTIME_EnableEvent(u8 enable) {
	if (enable) {
		RIT_ClearIntRequest();
		NVIC_SetPriority(RIT_IRQn, RIT_IRQ_PRIO);
		NVIC_EnableIRQ(RIT_IRQn);
	} else {
		NVIC_DisableIRQ(RIT_IRQn);
		RIT_ClearIntRequest();
	}
}

/** Get system time.
	@return current value of free running 32bit counter
 */
#define SYSTIME_Get() ((u32)(LPC_RITIMER->COUNTER))

/** Convert nanoseconds to systime ticks.
	@param ns time in nanoseconds
 */
#define SYSTIME_NsToTicks(ns) ((u32)(((u64)(ns)*SYSTIME_FRQ+500000000)/1000000000))

#if (SYSTIME_FRQ % 1000000 == 0)
/** Convert microseconds to systime ticks.
	@param ns time in microseconds
 */
#define SYSTIME_UsToTicks(us) ((u32)(us)*(SYSTIME_FRQ/1000000))
#else
/** Convert microseconds to systime ticks.
	@param ns time in microseconds
 */
#define SYSTIME_UsToTicks(us) ((u32)(((u64)(us)*SYSTIME_FRQ+500000)/1000000))
#endif

#if SYSTEMTIME_USE_INLINE
/** Wait for a given number of nanoseconds.
	note: at 100MHz, the 32bit counter overflows every 42.9s.
	@param ns time to wait in nanoseconds
 */
static __INLINE void SYSTIME_WaitNs(u32 ns) {
	u32 t1 = RIT_GetCounter();
	u32 delta = SYSTIME_NsToTicks(ns);
	while (SYSTIME_Get() - t1 < delta);
}

/** Wait for a given number of microseconds.
	note: at 100MHz, the 32bit counter overflows every 42.9s.
	@param us time to wait in microseconds
 */
static __INLINE void SYSTIME_WaitUs(u32 us) {
	u32 t1 = RIT_GetCounter();
	u32 delta = SYSTIME_UsToTicks(us);
	while (SYSTIME_Get() - t1 < delta);
}

/** Wait for a given number of milliseconds.
	note: at 100MHz, the 32bit counter overflows every 42.9s.
	@param ms time to wait in milliseconds
 */
static __INLINE void  SYSTIME_WaitMs(u32 ms) {
	u32 delta = SYSTIME_UsToTicks(1000);
	u32 t1 = RIT_GetCounter();
	while (ms > 0) {
		while (SYSTIME_Get() - t1 < delta);
		t1 += delta;
		ms--;
	}
}
#else
/** Wait for a given number of nanoseconds.
	note: at 100MHz, the 32bit counter overflows every 42.9s.
	@param ns time to wait in nanoseconds
 */
#define SYSTIME_WaitNs(ns) {             \
	u32 t1 = RIT_GetCounter();           \
	u32 delta = SYSTIME_NsToTicks(ns);   \
	while (SYSTIME_Get() - t1 < delta);  \
}

/** Wait for a given number of microseconds.
	note: at 100MHz, the 32bit counter overflows every 42.9s.
	@param us time to wait in microseconds
 */
#define SYSTIME_WaitUs(us) {             \
	u32 t1 = RIT_GetCounter();           \
	u32 delta = SYSTIME_UsToTicks(us);   \
	while (SYSTIME_Get() - t1 < delta);  \
}

/** Wait for a given number of milliseconds.
	note: at 100MHz, the 32bit counter overflows every 42.9s.
	@param ms time to wait in milliseconds
 */

#define SYSTIME_WaitMs(ms) {                \
	u32 delta = SYSTIME_UsToTicks(1000);    \
	u32 t1 = RIT_GetCounter();              \
	u32 msl = (ms);                         \
	while (msl > 0) {                       \
		while (SYSTIME_Get() - t1 < delta); \
		t1 += delta;                        \
		msl--;                              \
	}                                       \
}
#endif

#endif

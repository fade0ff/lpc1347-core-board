/** Basic type definitions
	Provide basic data types
	LPX13xx ARM Cortex M3
	----------------------------------------------------------
	Copyright 2012 Volker Oth
	Licensed under the Creative Commons Attribution 4.0 license
	http://creativecommons.org/licenses/by/4.0/

	Software is distributed on an "AS IS" BASIS, WITHOUT
	WARRANTIES OF ANY KIND, either express or implied.
*/

#ifndef TYPE_H
#define TYPE_H

#include "stdint.h"

typedef uint64_t u64; /**< Unsigned 64bit integer */
typedef int64_t  s64; /**< Signed 64bit integer */
typedef uint32_t u32; /**< Unsigned 32bit integer */
typedef int32_t  s32; /**< Signed 32bit integer */
typedef uint16_t u16; /**< Unsigned 16bit integer */
typedef int16_t  s16; /**< Signed 16bit integer */
typedef uint8_t  u8;  /**< Unsigned 8bit integer */
typedef int8_t   s8;  /**< Signed 8bit integer */

typedef u32 mutex_t;  /**< Handle for mutual exclude */

#endif

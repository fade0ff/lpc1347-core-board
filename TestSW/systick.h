/** System Tick library
	Access to SysTick timer
	LPX13xx ARM Cortex M3
	----------------------------------------------------------
	Copyright 2012 Volker Oth
	Licensed under the Creative Commons Attribution 4.0 license
	http://creativecommons.org/licenses/by/4.0/

	Software is distributed on an "AS IS" BASIS, WITHOUT
	WARRANTIES OF ANY KIND, either express or implied.
*/

#ifndef SYSTICK_H
#define SYSTICK_H

/* SysTick control register */

#define SYSTICK_CTRL_ENABLE     (1UL<<0)  /**< Enable the SysTick timer */
#define SYSTICK_CTRL_TICKINT    (1UL<<1)  /**< Enable SysTick exception request when counting down to 0 */
#define SYSTICK_CTRL_CLKSOURCE  (1UL<<2)  /**< Set clock source: 0 = external clock, 1 = processor clock. See SYSTICK_CLKSOURCE_xxx */
#define SYSTICK_CTRL_COUNTFLAG  (1UL<<16) /**< Returns 1 if timer counted to 0 since last time this was read */

/* Clock sources to be used with SYSTICK_CTRL_CLKSOURCE */
#define SYSTICK_CLKSOURCE_EXT   0UL
#define SYSTICK_CLKSOURCE_CPU   1UL

/* Functional Macros */

/** Enable/disable SysTick timer
	@param en 0: disable, 1: enable
 */
#define SYSTICK_Enable(en) {                  \
	if (en)                                   \
		SysTick->CTRL |= SYSTICK_CTRL_ENABLE; \
	else                                      \
		SysTick->CTRL &=~SYSTICK_CTRL_ENABLE; \
}

/** Enable/disable SysTick timer exception request when counting down to 0
	@param en 0: disable, 1: enable
 */
#define SYSTICK_EnableIRQ(en) {               \
	if (en)                                   \
		SysTick->CTRL |= SYSTICK_CTRL_TICKINT;\
	else                                      \
		SysTick->CTRL &=~SYSTICK_CTRL_TICKINT;\
}


/* Prototypes */

extern void SYSTICK_Init(void);


#endif

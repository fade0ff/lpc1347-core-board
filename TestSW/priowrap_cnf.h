/** Priority Wrapper - Configuration
	Provide means to wrap down an interrupt with higher priority to a base priority
	LPX13xx ARM Cortex M3
	----------------------------------------------------------
	Copyright 2012 Volker Oth
	Licensed under the Creative Commons Attribution 4.0 license
	http://creativecommons.org/licenses/by/4.0/

	Software is distributed on an "AS IS" BASIS, WITHOUT
	WARRANTIES OF ANY KIND, either express or implied.
*/

#ifndef PRIOWRAP__CNF_H
#define PRIOWRAP_CNF_H

#include "int_cnf.h"

#define PRIO_WRAP_MAX_ENTRIES 16 /**< maximum number of entries in the wrapper */

/** Create the interrupt used for priority wrapping (PendSV)
	Note: must be configured on low priority
*/
#define Prio_Wrap_Create_IRQ() { SCB->ICSR |= SCB_ICSR_PENDSVSET_Msk;}

/** Map PrioWrap handler to PendSV */
#define PrioWrap_Handler PendSV_Handler

/** Map PrioWrap interrupt to PendSV */
#define PrioWrap_IRQn    PendSV_IRQn

#define PRIOWRAP_IQR_PRIO WRAP_APP_IRQ_PRIO

#endif

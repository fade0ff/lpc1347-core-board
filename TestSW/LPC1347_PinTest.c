/*
===============================================================================
 Name        : LPC1347_PinTest.c
 Author      : $(author)
 Version     :
 Copyright   : $(copyright)
 Description : main definition
===============================================================================
*/

#include "global.h"
#include "sys.h"
#include "prc.h"
#include "pin.h"
#include "priowrap.h"
#include "systime.h"
#include "systick.h"

/* Exclude P0.10 (SWCLK) and P0.15 (SWDIO) - Pins > P0.23 don't exist */
#define PORT0_MASK ((1<<7)| (1<<6)| (1<<5)| (1<<4)| (1<<3)| (1<<2)| (1<<1)| (1<<0)| \
                   (0<<15)|(1<<14)|(1<<13)|(1<<12)|(1<<11)|(0<<10)| (1<<9)| (1<<8)| \
                   (1<<23)|(1<<22)|(1<<21)|(1<<20)|(1<<19)|(1<<18)|(1<<17)|(1<<16)| \
                   (0<<31)|(0<<30)|(0<<29)|(0<<28)|(0<<27)|(0<<26)|(0<<25)|(0<<24))

/* Pins P1.6, P1.9, P1.12 don't exist, P1.0..P1.5, P1.7..P1.11, P1.17..P1.18, P1.30 don't exist on LQFP48  */
#define PORT1_MASK ((0<<7)| (0<<6)| (0<<5)| (0<<4)| (0<<3)| (0<<2)| (0<<1)| (0<<0)| \
                   (1<<15)|(1<<14)|(1<<13)|(0<<12)|(0<<11)|(0<<10)| (0<<9)| (0<<8)| \
                   (1<<23)|(1<<22)|(1<<21)|(1<<20)|(1<<19)|(0<<18)|(0<<17)|(1<<16)| \
                   (1<<31)|(0<<30)|(1<<29)|(1<<28)|(1<<27)|(1<<26)|(1<<25)|(1<<24))


/** Millisecond counter */
volatile u32 ms_counter;
/** Pin Counter */
volatile u32 pin_counter;

/** 1ms low priority task
	@param dummy unused parameter needed for Prio_Wrap_Func_t
*/
void task_1ms(u32 dummy) {
	(void)dummy;

	if (pin_counter < 32) {
		LPC_GPIO->CLR[0] = 1<<pin_counter;
	} else if (pin_counter < 64) {
		LPC_GPIO->CLR[1] = 1<<(pin_counter&31);
	} else  if (pin_counter == 99) {
		// switch on all configured pins every 100ms
		pin_counter = -1;
		LPC_GPIO->PIN[0] = PORT0_MASK;
		LPC_GPIO->PIN[1] = PORT1_MASK;
	}
	pin_counter++;
}

/** 10ms low priority task
	@param dummy unused parameter needed for Prio_Wrap_Func_t
*/
void task_10ms(u32 dummy) {
	(void)dummy;
	// currently unused
}

/** 100ms low priority task
	@param dummy unused parameter needed for Prio_Wrap_Func_t
*/
void task_100ms(u32 dummy) {
	(void)dummy;
}

/** 1000ms low priority task
	@param dummy unused parameter needed for Prio_Wrap_Func_t
*/
void task_1000ms(u32 dummy) {
	(void)dummy;
	// currently unused
}

void SysTick_Handler(void) {
	PrioWrap_Function(task_1ms, 0);
	if (ms_counter % 10 == 0)
		PrioWrap_Function(task_10ms, 0);
	if (ms_counter % 100 == 0)
		PrioWrap_Function(task_100ms, 0);
	if (ms_counter % 1000 == 0)
		PrioWrap_Function(task_1000ms, 0);

	ms_counter++;
}


int main(void) {
	// Configure pins to GPIO that are not GPIO by default
	LPC_IOCON->RESET_PIO0_0 = 1;
	// PIO0_10 and PIO0_15 are per default SWCLK and SWDIO - keep this to be able to reprogram
	LPC_IOCON->TDI_PIO0_11 = 1;
	LPC_IOCON->TMS_PIO0_12 = 1;
	LPC_IOCON->TDO_PIO0_13 = 1;
	LPC_IOCON->TRST_PIO0_14 = 1;

	// Configure pins as output
	LPC_GPIO->DIR[0] = PORT0_MASK;
	LPC_GPIO->DIR[1] = PORT1_MASK;
	// switch all configured pins on
	LPC_GPIO->PIN[0] = PORT0_MASK;
	LPC_GPIO->PIN[1] = PORT1_MASK;

	SYSTIME_Init();                         /* init system timer */

	// Init timebase
	// SysTick and PendSV are system exceptions - they don't need to be enabled
	PrioWrap_Init();
	NVIC_SetPriority(SysTick_IRQn, SYSTICK_IRQ_PRIO);   /* set to medium prio */
	SYSTICK_Init();                                     /* 1ms system tick */
	SYSTICK_Enable(1);
	SYSTICK_EnableIRQ(1);

    while(1) {

    }

}


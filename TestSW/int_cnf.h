/** INT library - Configuration
	Configuration of interrupt levels
	LPX13xx ARM Cortex M3
	----------------------------------------------------------
	Copyright 2012 Volker Oth
	Licensed under the Creative Commons Attribution 4.0 license
	http://creativecommons.org/licenses/by/4.0/

	Software is distributed on an "AS IS" BASIS, WITHOUT
	WARRANTIES OF ANY KIND, either express or implied.
*/

#ifndef INT_CNF_H
#define INT_CNF_H

/** define all interrupt levels here (0 is the highest, 31 the lowest prio) */

#define RIT_IRQ_PRIO            1 /**< Handler for RIT event handler */
#define SSP_IRQ_PRIO            4 /**< Handler for SSP */
#define USB_IRQ_PRIO		    8 /**< USB receive interrupt */
#define SYSTICK_IRQ_PRIO        8 /**< Handler for time base creation - same prio as USB to avoid issues */
#define UART_IRQ_PRIO          10 /**< UART receive interrupt */
#define WRAP_APP_IRQ_PRIO      30 /**< Wrapper interrupt used to wrap down to low priority */

#endif

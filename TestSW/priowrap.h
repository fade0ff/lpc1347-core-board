/** Priority Wrapper
	Provide means to wrap down an interrupt with higher priority to a base priority
	LPX13xx ARM Cortex M3
	----------------------------------------------------------
	Copyright 2012 Volker Oth
	Licensed under the Creative Commons Attribution 4.0 license
	http://creativecommons.org/licenses/by/4.0/

	Software is distributed on an "AS IS" BASIS, WITHOUT
	WARRANTIES OF ANY KIND, either express or implied.
*/

#ifndef PRIOWRAP_H
#define PRIOWRAP_H

#include "priowrap_cnf.h"

/** Function pointer used to wrap functions */
typedef void (*Prio_Wrap_Func_t)(const u32 param);

/** Table entry for priority wrapper */
typedef struct {
   Prio_Wrap_Func_t func;            /**< pointer to function */
   u32 param;                        /**< parameter */
} Prio_Wrap_t;

/** RAM Table for priority wrapper - used to store wrapped functions */
typedef struct {
   volatile Prio_Wrap_t entries[PRIO_WRAP_MAX_ENTRIES];  /**<  pointer list to the pending wrapped functions  */
   volatile u8 idx;                                      /**<  current index in table */
} Prio_Wrap_Table_t;

extern int PrioWrap_Function(Prio_Wrap_Func_t func, u32 param);
extern void PrioWrap_Handler(void);
extern void PrioWrap_Init(void);

#endif

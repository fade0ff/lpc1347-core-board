/** Pin Access
	Configure pin function / direction / settings, read input pins, write output pins
	LPX13xx ARM Cortex M3
	----------------------------------------------------------
	Copyright 2012 Volker Oth
	Licensed under the Creative Commons Attribution 4.0 license
	http://creativecommons.org/licenses/by/4.0/

	Software is distributed on an "AS IS" BASIS, WITHOUT
	WARRANTIES OF ANY KIND, either express or implied.
*/

#ifndef PIN_H
#define PIN_H

#include "pin_cnf.h"

/* Pin access via channel identifiers */

/** Get port from channel identifier c */
#define PIN_CH_PORT(ch)   ( (u8)((ch) >> 8) )
/** Get pin from channel identifier c */
#define PIN_CH_PIN(ch)    ( (ch) & 0xff )
/** Create channel identifier from port and pin */
#define PIN_CH(port,pin) (((port)<<8) | pin)


/** Set pin direction through channel identifier.
	@param ch pin channel identifier (created via PIN_CH)
	@param level 0:lo or 1:hi
 */
#define PIN_ChSet(ch, value) {                                \
	if ((value)==0)                                           \
		LPC_GPIO->CLR[PIN_CH_PORT(ch)] = 1UL<<PIN_CH_PIN(ch); \
	else                                                      \
		LPC_GPIO->SET[PIN_CH_PORT(ch)] = 1UL<<PIN_CH_PIN(ch); \
}

/** Read pin input through channel identifier.
	@param ch pin channel identifier (created via PIN_CH)
	@return level 0:lo or 1:hi
 */
#define PIN_ChGet(ch) (LPC_GPIO->PIN[PIN_CH_PORT(ch)] & (1UL<<PIN_CH_PIN(ch)) ) != 0)?1:0)

/** Set pin direction through channel identifier.
	@param ch pin channel identifier (created via PIN_CH)
	@param dir 0 for input, 1 for output
 */
#define PIN_ChSetDir(ch, dir) {                                        \
	if ((value)==0)                                                    \
		LPC_GPIO->DIR[PIN_CH_PORT(ch)] &= (u32)~(1UL<<PIN_CH_PIN(ch)); \
	else                                                               \
		LPC_GPIO->DIR[PIN_CH_PORT(ch)] |= (1UL<<PIN_CH_PIN(ch));       \
}

/* Channel definitions */

#define PIN_0_0  PIN_CH(0, 0)
#define PIN_0_1  PIN_CH(0, 1)
#define PIN_0_2  PIN_CH(0, 2)
#define PIN_0_3  PIN_CH(0, 3)
#define PIN_0_4  PIN_CH(0, 4)
#define PIN_0_5  PIN_CH(0, 5)
#define PIN_0_6  PIN_CH(0, 6)
#define PIN_0_7  PIN_CH(0, 7)
#define PIN_0_8  PIN_CH(0, 8)
#define PIN_0_9  PIN_CH(0, 9)
#define PIN_0_10 PIN_CH(0,10)
#define PIN_0_11 PIN_CH(0,11)
#define PIN_0_15 PIN_CH(0,15)
#define PIN_0_16 PIN_CH(0,16)
#define PIN_0_17 PIN_CH(0,17)
#define PIN_0_18 PIN_CH(0,18)
#define PIN_0_19 PIN_CH(0,19)
#define PIN_0_20 PIN_CH(0,20)
#define PIN_0_21 PIN_CH(0,21)
#define PIN_0_22 PIN_CH(0,22)
#define PIN_0_23 PIN_CH(0,23)

#define PIN_1_0  PIN_CH(1, 0)
#define PIN_1_1  PIN_CH(1, 1)
#define PIN_1_2  PIN_CH(1, 2)
#define PIN_1_3  PIN_CH(1, 3)
#define PIN_1_4  PIN_CH(1, 4)
#define PIN_1_5  PIN_CH(1, 5)
#define PIN_1_7  PIN_CH(1, 7)
#define PIN_1_8  PIN_CH(1, 8)
#define PIN_1_10 PIN_CH(1,10)
#define PIN_1_11 PIN_CH(1,11)
#define PIN_1_13 PIN_CH(1,13)
#define PIN_1_14 PIN_CH(1,14)
#define PIN_1_15 PIN_CH(1,15)
#define PIN_1_16 PIN_CH(1,16)
#define PIN_1_17 PIN_CH(1,17)
#define PIN_1_18 PIN_CH(1,18)
#define PIN_1_19 PIN_CH(1,19)
#define PIN_1_20 PIN_CH(1,20)
#define PIN_1_21 PIN_CH(1,21)
#define PIN_1_22 PIN_CH(1,22)
#define PIN_1_23 PIN_CH(1,23)
#define PIN_1_24 PIN_CH(1,24)
#define PIN_1_25 PIN_CH(1,25)
#define PIN_1_26 PIN_CH(1,26)
#define PIN_1_27 PIN_CH(1,27)
#define PIN_1_28 PIN_CH(1,28)
#define PIN_1_29 PIN_CH(1,29)
#define PIN_1_31 PIN_CH(1,31)


/* Port channel access macros */
#define PIN_PORT_0 0
#define PIN_PORT_1 1


/** Pin Initialization - configures all pins as specified in pin_cnf.h */
#define PIN_Init() {                                     \
		LPC_GPIO->PIN[0]         = PIN_PORT0_LVL_CFG;    \
		LPC_GPIO->PIN[1]         = PIN_PORT1_LVL_CFG;    \
		LPC_IOCON->RESET_PIO0_0  = PIN_IOCON_P0_0_CFG;   \
		LPC_IOCON->PIO0_1        = PIN_IOCON_P0_1_CFG;   \
		LPC_IOCON->PIO0_2        = PIN_IOCON_P0_2_CFG;   \
		LPC_IOCON->PIO0_3        = PIN_IOCON_P0_3_CFG;   \
		LPC_IOCON->PIO0_4        = PIN_IOCON_P0_4_CFG;   \
		LPC_IOCON->PIO0_5        = PIN_IOCON_P0_5_CFG;   \
		LPC_IOCON->PIO0_6        = PIN_IOCON_P0_6_CFG;   \
		LPC_IOCON->PIO0_7        = PIN_IOCON_P0_7_CFG;   \
		LPC_IOCON->PIO0_8        = PIN_IOCON_P0_8_CFG;   \
		LPC_IOCON->PIO0_9        = PIN_IOCON_P0_9_CFG;   \
		LPC_IOCON->SWCLK_PIO0_10 = PIN_IOCON_P0_10_CFG;  \
		LPC_IOCON->TDI_PIO0_11   = PIN_IOCON_P0_11_CFG;  \
		LPC_IOCON->TMS_PIO0_12   = PIN_IOCON_P0_12_CFG;  \
		LPC_IOCON->TDO_PIO0_13   = PIN_IOCON_P0_13_CFG;  \
		LPC_IOCON->TRST_PIO0_14  = PIN_IOCON_P0_14_CFG;  \
		LPC_IOCON->SWDIO_PIO0_15 = PIN_IOCON_P0_15_CFG;  \
		LPC_IOCON->PIO0_16       = PIN_IOCON_P0_16_CFG;  \
		LPC_IOCON->PIO0_17       = PIN_IOCON_P0_17_CFG;  \
		LPC_IOCON->PIO0_18       = PIN_IOCON_P0_18_CFG;  \
		LPC_IOCON->PIO0_19       = PIN_IOCON_P0_19_CFG;  \
		LPC_IOCON->PIO0_20       = PIN_IOCON_P0_20_CFG;  \
		LPC_IOCON->PIO0_21       = PIN_IOCON_P0_21_CFG;  \
		LPC_IOCON->PIO0_22       = PIN_IOCON_P0_22_CFG;  \
		LPC_IOCON->PIO0_23       = PIN_IOCON_P0_23_CFG;  \
		LPC_IOCON->PIO1_0        = PIN_IOCON_P1_0_CFG;   \
		LPC_IOCON->PIO1_1        = PIN_IOCON_P1_1_CFG;   \
		LPC_IOCON->PIO1_2        = PIN_IOCON_P1_2_CFG;   \
		LPC_IOCON->PIO1_3        = PIN_IOCON_P1_3_CFG;   \
		LPC_IOCON->PIO1_4        = PIN_IOCON_P1_4_CFG;   \
		LPC_IOCON->PIO1_5        = PIN_IOCON_P1_5_CFG;   \
		LPC_IOCON->PIO1_7        = PIN_IOCON_P1_7_CFG;   \
		LPC_IOCON->PIO1_8        = PIN_IOCON_P1_8_CFG;   \
		LPC_IOCON->PIO1_10       = PIN_IOCON_P1_10_CFG;  \
		LPC_IOCON->PIO1_11       = PIN_IOCON_P1_11_CFG;  \
		LPC_IOCON->PIO1_13       = PIN_IOCON_P1_13_CFG;  \
		LPC_IOCON->PIO1_14       = PIN_IOCON_P1_14_CFG;  \
		LPC_IOCON->PIO1_15       = PIN_IOCON_P1_15_CFG;  \
		LPC_IOCON->PIO1_16       = PIN_IOCON_P1_16_CFG;  \
		LPC_IOCON->PIO1_17       = PIN_IOCON_P1_17_CFG;  \
		LPC_IOCON->PIO1_18       = PIN_IOCON_P1_18_CFG;  \
		LPC_IOCON->PIO1_19       = PIN_IOCON_P1_19_CFG;  \
		LPC_IOCON->PIO1_20       = PIN_IOCON_P1_20_CFG;  \
		LPC_IOCON->PIO1_21       = PIN_IOCON_P1_21_CFG;  \
		LPC_IOCON->PIO1_22       = PIN_IOCON_P1_22_CFG;  \
		LPC_IOCON->PIO1_23       = PIN_IOCON_P1_23_CFG;  \
		LPC_IOCON->PIO1_24       = PIN_IOCON_P1_24_CFG;  \
		LPC_IOCON->PIO1_25       = PIN_IOCON_P1_25_CFG;  \
		LPC_IOCON->PIO1_26       = PIN_IOCON_P1_26_CFG;  \
		LPC_IOCON->PIO1_27       = PIN_IOCON_P1_27_CFG;  \
		LPC_IOCON->PIO1_28       = PIN_IOCON_P1_28_CFG;  \
		LPC_IOCON->PIO1_29       = PIN_IOCON_P1_29_CFG;  \
		LPC_IOCON->PIO1_31       = PIN_IOCON_P1_31_CFG;  \
		LPC_GPIO->DIR[0]         = PIN_PORT0_DIR_CFG;    \
		LPC_GPIO->DIR[1]         = PIN_PORT1_DIR_CFG;    \
}



/* Note: everything below this point is just to automatically create the macros for PIN_Init() */
#define PIN_PORT0_LVL_CFG (PIN_P0_0_LVL_CFG       |(PIN_P0_1_LVL_CFG<<1)  |(PIN_P0_2_LVL_CFG<<2)  |(PIN_P0_3_LVL_CFG<<3)  |(PIN_P0_4_LVL_CFG<<4)|(PIN_P0_5_LVL_CFG<<5)|     \
                           (PIN_P0_6_LVL_CFG<<6)  |(PIN_P0_7_LVL_CFG<<7)  |(PIN_P0_8_LVL_CFG<<8)  |(PIN_P0_9_LVL_CFG<<9)  |(PIN_P0_10_LVL_CFG<<10)|(PIN_P0_11_LVL_CFG<<11)| \
                           (PIN_P0_12_LVL_CFG<<12)|(PIN_P0_13_LVL_CFG<<13)|(PIN_P0_14_LVL_CFG<<14)|(PIN_P0_15_LVL_CFG<<15)|(PIN_P0_16_LVL_CFG<<16)|(PIN_P0_17_LVL_CFG<<17)| \
                           (PIN_P0_18_LVL_CFG<<18)|(PIN_P0_19_LVL_CFG<<19)|(PIN_P0_20_LVL_CFG<<20)|(PIN_P0_21_LVL_CFG<<21)|(PIN_P0_22_LVL_CFG<<22)|(PIN_P0_23_LVL_CFG<<23))

#define PIN_PORT1_LVL_CFG (PIN_P1_0_LVL_CFG       |(PIN_P1_1_LVL_CFG<<1)  |(PIN_P1_2_LVL_CFG<<2)  |(PIN_P1_3_LVL_CFG<<3)  |(PIN_P1_4_LVL_CFG<<4)  |(PIN_P1_5_LVL_CFG<<5)  |(PIN_P1_7_LVL_CFG<<7)   | \
                           (PIN_P1_8_LVL_CFG<<8)  |(PIN_P1_10_LVL_CFG<<10)|(PIN_P1_11_LVL_CFG<<11)|(PIN_P1_13_LVL_CFG<<13)|(PIN_P1_14_LVL_CFG<<14)|(PIN_P1_15_LVL_CFG<<15)|(PIN_P1_16_LVL_CFG<<16) | \
                           (PIN_P1_17_LVL_CFG<<17)|(PIN_P1_18_LVL_CFG<<18)|(PIN_P1_19_LVL_CFG<<19)|(PIN_P1_20_LVL_CFG<<20)|(PIN_P1_21_LVL_CFG<<21)|(PIN_P1_22_LVL_CFG<<22)|(PIN_P1_23_LVL_CFG<<23) | \
                           (PIN_P1_24_LVL_CFG<<24)|(PIN_P1_25_LVL_CFG<<25)|(PIN_P1_26_LVL_CFG<<26)|(PIN_P1_27_LVL_CFG<<27)|(PIN_P1_28_LVL_CFG<<28)|(PIN_P1_29_LVL_CFG<<29)|(PIN_P1_31_LVL_CFG<<31))

#define PIN_PORT0_DIR_CFG (PIN_P0_0_DIR_CFG       |(PIN_P0_1_DIR_CFG<<1)  |(PIN_P0_2_DIR_CFG<<2)  |(PIN_P0_3_DIR_CFG<<3)  |(PIN_P0_4_DIR_CFG<<4)|(PIN_P0_5_DIR_CFG<<5)|     \
                           (PIN_P0_6_DIR_CFG<<6)  |(PIN_P0_7_DIR_CFG<<7)  |(PIN_P0_8_DIR_CFG<<8)  |(PIN_P0_9_DIR_CFG<<9)  |(PIN_P0_10_DIR_CFG<<10)|(PIN_P0_11_DIR_CFG<<11)| \
                           (PIN_P0_12_DIR_CFG<<12)|(PIN_P0_13_DIR_CFG<<13)|(PIN_P0_14_DIR_CFG<<14)|(PIN_P0_15_DIR_CFG<<15)|(PIN_P0_16_DIR_CFG<<16)|(PIN_P0_17_DIR_CFG<<17)| \
                           (PIN_P0_18_DIR_CFG<<18)|(PIN_P0_19_DIR_CFG<<19)|(PIN_P0_20_DIR_CFG<<20)|(PIN_P0_21_DIR_CFG<<21)|(PIN_P0_22_DIR_CFG<<22)|(PIN_P0_23_DIR_CFG<<23))

#define PIN_PORT1_DIR_CFG (PIN_P1_0_DIR_CFG       |(PIN_P1_1_DIR_CFG<<1)  |(PIN_P1_2_DIR_CFG<<2)  |(PIN_P1_3_DIR_CFG<<3)  |(PIN_P1_4_DIR_CFG<<4)  |(PIN_P1_5_DIR_CFG<<5)  |(PIN_P1_7_DIR_CFG<<7)   | \
                           (PIN_P1_8_DIR_CFG<<8)  |(PIN_P1_10_DIR_CFG<<10)|(PIN_P1_11_DIR_CFG<<11)|(PIN_P1_13_DIR_CFG<<13)|(PIN_P1_14_DIR_CFG<<14)|(PIN_P1_15_DIR_CFG<<15)|(PIN_P1_16_DIR_CFG<<16) | \
                           (PIN_P1_17_DIR_CFG<<17)|(PIN_P1_18_DIR_CFG<<18)|(PIN_P1_19_DIR_CFG<<19)|(PIN_P1_20_DIR_CFG<<20)|(PIN_P1_21_DIR_CFG<<21)|(PIN_P1_22_DIR_CFG<<22)|(PIN_P1_23_DIR_CFG<<23) | \
                           (PIN_P1_24_DIR_CFG<<24)|(PIN_P1_25_DIR_CFG<<25)|(PIN_P1_26_DIR_CFG<<26)|(PIN_P1_27_DIR_CFG<<27)|(PIN_P1_28_DIR_CFG<<28)|(PIN_P1_29_DIR_CFG<<29)|(PIN_P1_31_DIR_CFG<<31))

/* Macros to access the configuration macros from pin_cnf.h */

#define PIN_IOCON_FUNC0             0
#define PIN_IOCON_FUNC1             1
#define PIN_IOCON_FUNC2             2
#define PIN_IOCON_FUNC3             3

#define PIN_IOCON_NOPULL       (0<<3)
#define PIN_IOCON_PULLDOWN     (1<<3)
#define PIN_IOCON_PULLUP       (2<<3)
#define PIN_IOCON_REPEATER     (3<<3)

#define PIN_IOCON_NO_HYS       (0<<5)
#define PIN_IOCON_HYS          (1<<5)

#define PIN_IOCON_NO_INV       (0<<6)
#define PIN_IOCON_INV          (1<<6)

#define PIN_IOCON_ANALOG       (0<<7)
#define PIN_IOCON_DIGITAL      (1<<7)

#define PIN_IOCON_I2C_FAST     (0<<8)
#define PIN_IOCON_I2C_STANDARD (1<<8)
#define PIN_IOCON_I2C_FASTPLUS (2<<8)

#define PIN_IOCON_FILTR_ON     (0<<8)
#define PIN_IOCON_FILTR_OFF    (1<<8)

#define PIN_IOCON_NO_OD        (0<<10)
#define PIN_IOCON_OD           (1<<10)

#endif


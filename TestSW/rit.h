/** RIT library
	Repetitive Interrupt Timer
	LPX13xx ARM Cortex M3
	----------------------------------------------------------
	Copyright 2012 Volker Oth
	Licensed under the Creative Commons Attribution 4.0 license
	http://creativecommons.org/licenses/by/4.0/

	Software is distributed on an "AS IS" BASIS, WITHOUT
	WARRANTIES OF ANY KIND, either express or implied.
*/

#ifndef RIT_H
#define RIT_H

#include "sys.h"
#include "prc.h"

/** Set RIT mask register.
	@param m Mask to remove bits from comparison - usually leave it to 0
 */
#define RIT_SetMask(m) {LPC_RITIMER->MASK = (m);}

/** Set RIT compare register.
	@param c value to write to compare register
 */
#define RIT_SetCompare(c) {LPC_RITIMER->COMPVAL = (c);}

/** Get RIT compare register.
	@return RICOMPVAL register
 */
#define RIT_GetCompare(c) (LPC_RIT->COMPVAL)

#define RIT_CTRL_INT_REQUEST (1<<0)  /**< interrupt request set by HW - write 1 to clear */
#define RIT_CTRL_CLR         (1<<1)  /**< if set, the counter is cleared on compare event */
#define RIT_CTRL_HALT_DBG    (1<<2)  /**< if set, the timer is stopped while CPU is stopped for debugging */
#define RIT_CTRL_ENABLE      (1<<3)  /**< if set, the timer is enabled */

/** Set RIT control register.
	@param c value to write to control register (construct from RIT_CTRL_xxx macros)
 */
#define RIT_SetControl(c) {LPC_RITIMER->CTRL = (c);}

/** Get RIT control register.
	@return RICTRL register
 */
#define RIT_GetControl()  (LPC_RITIMER->CTRL)

/** Clear RIT interrupt request. */
#define RIT_ClearIntRequest() {BITBAND_SET_BIT((u32)&LPC_RITIMER->CTRL,0,1);}

/** Set RIT counter.
	@param c value to write to counter
 */
#define RIT_SetCounter(c) {LPC_RITIMER->COUNTER = (c);}

/** Get RIT counter.
	@return RICOUNTER register
 */
#define RIT_GetCounter(c) (LPC_RITIMER->COUNTER)

#endif

/** System configuration - Configuration
	Configuration of system resources (CPU clock, prescalers etc.)
	LPX13xx ARM Cortex M3
	----------------------------------------------------------
	Copyright 2012 Volker Oth
	Licensed under the Creative Commons Attribution 4.0 license
	http://creativecommons.org/licenses/by/4.0/

	Software is distributed on an "AS IS" BASIS, WITHOUT
	WARRANTIES OF ANY KIND, either express or implied.
*/


#ifndef SYS_CNF_H
#define SYS_CNF_H

/* Peripheral enable */
#define SYSAHBCLKCTRL_CFG ( \
		SYS_CLK_SYS_CFG(1)        | SYS_CLK_ROM_CFG(1)      | SYS_CLK_RAM0_CFG(1)     | SYS_CLK_FLASHREG_CFG(0) | \
		SYS_CLK_FLASHARRAY_CFG(0) | SYS_CLK_I2C_CFG(0)      | SYS_CLK_GPIO_CFG(1)     | SYS_CLK_CT16B0_CFG(1)   | \
		SYS_CLK_CT16B1_CFG(1)     | SYS_CLK_CT32B0_CFG(1)   | SYS_CLK_CT32B1_CFG(1)   | SYS_CLK_SSP0_CFG(1)     | \
		SYS_CLK_USART_CFG(1)      | SYS_CLK_ADC_CFG(1)      | SYS_CLK_USB_CFG(1)      | SYS_CLK_WWDT_CFG(0)     | \
		SYS_CLK_IOCON_CFG(1)      | SYS_CLK_SSP1_CFG(1)     | SYS_CLK_PINT_CFG(0)     | SYS_CLK_GROUP0INT_CFG(0)| \
		SYS_CLK_GROUP1INT_CFG(0)  | SYS_CLK_RAM1_CFG(1)     | SYS_CLK_USBSRAM_CFG(1))

/* Peripheral reset control (0: reset, 1: reset de-asserted) */
#define SYS_PRESETCTRL_CFG ( SYS_PRESETCTRL_SSP0_RST_N_CFG(1) | SYS_PRESETCTRL_I2C_RST_N_CFG(1) | SYS_PRESETCTRL_SSP1_RST_N_CFG(1) )

/** Peripheral clock dividers 0..255 - 0 to disable */
#define SYS_CLK_SSP0_DIV    1
#define SYS_CLK_SSP1_DIV    1
#define SYS_CLK_TRACE_DIV   1
#define SYS_CLK_SYSTICK_DIV 1
#define SYS_CLK_UART_DIV    1

/* Frequency configuration */
#define SYS_FRQ_CORE  72000000UL       /**< Desired core frequency 72 MHz - has to match SYS_FRQ_PLL and SYS_CLK_DIV */
#define SYS_FRQ_OSC   12000000UL       /**< Oscillator/Crystal frequency */

/* PLL output frequency SYS_FRQ_PLL
   SYS_FRQ_PLL = SYS_FRQ_OSC*SYS_MSEL_CFG
*/
#define SYS_MSEL_CFG       6           /**< PLL multiplier [1..32] */
#define SYS_PSEL_CFG       2           /**< PLL divider  [1,2,4,8] */

/* The CPU core frequency is SYS_FRQ_PLL/SYS_CLK_DIV */
#define SYS_CLK_DIV        1           /**< CLock divider [1..256] */

/* USB PLL output frequency should always be 48MHz (USB)
 */
#define SYS_MSEL_USB_CFG   4           /**< PLL1 multiplier [1 .. 32] */
#define SYS_PSEL_USB_CFG   2           /**< PLL divider [1,2,4,8] */

/* The USB frequency is SYS_FRQ_USB_PLL/SYS_CLK_DIV */
#define SYS_USB_CLK_DIV    1           /**< CLock divider [1..256] */


#endif
